#!/bin/sh
# Clone and update all projects in the "GNOME" group on gitlab.gnome.org
#
# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode
# Author: Andre Klapper
#
# Docs at https://docs.gitlab.com/ee/api/projects.html#list-all-projects

if [ -z "$GNOME_GITLAB_PERSONAL_ACCESS_TOKEN" ]; then
  echo "Please set the GNOME_GITLAB_PERSONAL_ACCESS_TOKEN environment variable, e.g. in your ~/.bashrc file. See https://gitlab.gnome.org/profile/personal_access_tokens"
  exit 1
fi

echo "NOTE: Make sure to use a GitLab token for API access."
echo "A token for Read access only might not pull all repos."

FILENAME="gnome-repos-list.tmp"

# https://docs.gitlab.com/ce/api/#pagination-link-header
TOTALPAGES=`curl -s --head https://gitlab.gnome.org/api/v4/groups/GNOME/projects?private_token=$GNOME_GITLAB_PERSONAL_ACCESS_TOKEN\&per_page=100 -I | grep -Fi X-Total-Pages | sed 's/[^ ]* //' | tr -d '\r'`

#explicitly match for path_with_namespace NOT to clone projects which were moved to a different namespace in the meantime
curl -s "https://gitlab.gnome.org/api/v4/groups/GNOME/projects?private_token=$GNOME_GITLAB_PERSONAL_ACCESS_TOKEN&per_page=100&page=$i" | jq -r '.[] | select(.path_with_namespace | contains("GNOME/")).path' > "$FILENAME"

for i in $( seq 2 $TOTALPAGES )
do
  curl -s "https://gitlab.gnome.org/api/v4/groups/GNOME/projects?private_token=$GNOME_GITLAB_PERSONAL_ACCESS_TOKEN&per_page=100&page=$i" | jq -r '.[] | select(.path_with_namespace | contains("GNOME/")).path' >> "$FILENAME"
  sleep 20
done

mkdir -p GNOME/
cd GNOME/

while read repo; do
  # For anonymous checkout, use GITURL="https://gitlab.gnome.org/GNOME/$repo.git" instead
  GITURL="ssh://git@gitlab.gnome.org/GNOME/$repo.git"
  if [ ! -d $repo ]; then
    echo "Cloning $repo"
    git clone "$GITURL"
  else
    echo "Updating $repo"
#    (cd "$repo" && git checkout master && git pull --rebase)
    cd "$repo"
    if [[ $(git rev-parse --verify --quiet main) ]]; then
      git checkout main
    elif [[ $(git rev-parse --verify --quiet master) ]]; then
      git checkout master
    fi
    git pull --rebase
    cd ..
  fi
done < "../$FILENAME"
cd ..

rm "$FILENAME"

# Only print a list of all project names (which might include spaces):
# curl -s "https://gitlab.gnome.org/api/v4/groups/GNOME/projects?private_token=$GNOME_GITLAB_PERSONAL_ACCESS_TOKEN" | jq -r '.[] | .name'
